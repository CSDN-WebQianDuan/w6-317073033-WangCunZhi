//album 相册
//album index 相册目录
//album index1 为第点击相册目录对应的相册的标记
//pageNumber相册的页数
//有一次不知道怎么样弄的 同一张图片可以上上传很多次 现在第二次点同一张图片不行 还是只显示一张图片
//page.index3 the page index


//第一次加载网页时 选择的是第一个相册 and show the first page of the album
var allAlbumIndex=document.querySelectorAll('.albumbox, .chose');//相册列表
var allAlbum=document.querySelectorAll('.hidden, .show');
var choseAlbumIndex=allAlbumIndex[0];//选择的标签
var choseAlbum=allAlbum[0];//对应的选择相册
var allPage=choseAlbum.querySelectorAll("a");//
var chosePage=allPage[0];
var allImage=choseAlbum.querySelectorAll("img");
var allImageBox=choseAlbum.querySelectorAll('.picturebox, .pictureboxhidden');
choseAlbumIndex.className="chose";//choose the first album
choseAlbum.className="show";//打开网页的时候第一个相册显示
chosePage.className="achose";//and choosed the first page of the album

allAlbumIndex.forEach((a,index)=>{
  a.index=index;
  a.addEventListener("click",clickalbumindex_handle);
})


allAlbum.forEach((div,index)=>{//add index1 for Album, index3 for page, add listen for page index
  div.index1=index;
  allPage=div.querySelectorAll("a");
  allPage.forEach((a,index)=>{
    a.index3=index;
    a.addEventListener("click",clickpage_handle)
  })
})

//处理点击处理相册目录事件函数
function clickalbumindex_handle(event){
  var index=event.target.index;
  allAlbumIndex=document.querySelectorAll('.albumbox, .chose');//相册目录列表
  allAlbum=document.querySelectorAll('.hidden, .show');
  renderalbum(index);
}

//切换到点击的相册目录标签
function renderalbum(index){

		choseAlbumIndex.className="albumbox";//上次选择的相册标签标记为未选择
    choseAlbumIndex=allAlbumIndex[index];//本次选择的相册目录
    choseAlbumIndex.className="chose";//本次选择的相侧标签标记为选择
    choseAlbum.className="hidden";//隐藏上次选择的相册
    allAlbum[index].className="show";//显示本次选择的相册页面
    choseAlbum=allAlbum[index];////显示本次选择的相册页面
    renderpage(0);////choose the first page of a album when we choose a new album


}

//处理加入相册目录标签
const addAlbumButton=document.querySelector("#addalbumbutton");

addAlbumButton.addEventListener("click",addnewalbum);
//添加一个新相册 可以用 albumn.uniqueId = "id"+(++currentId);给相册目录设置标签
function addnewalbum(){
  const newalbumname1=prompt("Please enther album name!","New Album");//prompt box enther new album name
  if (newalbumname1!=null)
  {
  const newAlbumIndex=document.createElement("a");//创建一个新的相册标签
  newAlbumIndex.className="albumbox";//相册标签的属性
  newAlbumIndex.href="#";//相册的链接
  const newAlbumNode=document.createTextNode(newalbumname1);//创建新相册名称
  newAlbumIndex.appendChild(newAlbumNode);//写新相册名称
  allAlbumIndex=document.querySelectorAll('.albumbox, .chose');//选择左边相册标签列表；
  const lastAlbumIndex=allAlbumIndex[allAlbumIndex.length-1];
  lastAlbumIndex.insertAdjacentElement('afterend',newAlbumIndex);//加入新相册标签
  newAlbumIndex.index=allAlbumIndex.length;
  newAlbumIndex.addEventListener("click",clickalbumindex_handle);//listen

  const newAlbum=document.createElement("div");//创建一个新的相册
  newAlbum.className="hidden";
  allAlbumt=document.querySelectorAll('.hidden, .show');//all album before add
  newAlbum.index1=allAlbum.length;//给新添加的相册添加一个index1属性，和相册目录对应
  newAlbum.pageNumber=1;//album's initial pageNumber is 1

  const newFooter=document.createElement("div");//creat the first page of a album
  newFooter.className="footer";
  const newPageA=document.createElement("a");
  //newPageA.className="achose";
  newPageA.href="#";
  newPageA.index3=0;
  newPageA.innerHTML="1";
  //chosePage=newPageA;
  newFooter.appendChild(newPageA);
  newAlbum.appendChild(newFooter);
  const lastAlbum=allAlbum[allAlbum.length-1];
  lastAlbum.insertAdjacentElement('afterend',newAlbum);
  newPageA.addEventListener("click",clickpage_handle)//listen

  allAlbumIndex=document.querySelectorAll('.albumbox, .chose');//相册目录列表
  allAlbum=document.querySelectorAll('.hidden, .show');
  console.log(newAlbumIndex.index);
  renderalbum(newAlbumIndex.index);//choose the new album
  }
}



function renderpage(index){
  chosePage.className="";
  for(let i=chosePage.index3*4;i<chosePage.index3*4+4&&i<allImageBox.length;i++){
    allImageBox[i].className="pictureboxhidden";
  }
  allPage=choseAlbum.querySelectorAll("a");//
  chosePage=allPage[index];
  chosePage.className="achose";
  allImageBox=choseAlbum.querySelectorAll('.pictureboxhidden, .picturebox');

  for(let i=0;i<allImageBox.length;i++){
    allImageBox[i].className="pictureboxhidden";
  }
  for(let i=chosePage.index3*4;i<chosePage.index3*4+4&&i<allImageBox.length;i++){
    allImageBox[i].className="picturebox";
  }


}

//pageNumber 相册的页数
//currentPage;the chose page of the current album


const fileInput = document.querySelector("input[type='file']");
fileInput.addEventListener("change",function(event){

  allPage=choseAlbum.querySelectorAll("a");
  var currentPagesNumber=allPage.length;


//upload a picture
  const file = fileInput.files[0];//获取上传文件信息
  const fr = new FileReader();//生成读取器
  fr.onload = function() {//读取成功后调用
    const img1 = document.createElement("img");
    img1.src = fr.result;//数据结果
    const picturebox1=document.createElement("div");
    picturebox1.className="picturebox";
    picturebox1.appendChild(img1);
    choseAlbum.appendChild(picturebox1);
    allImageBox=choseAlbum.querySelectorAll('.picturebox, .pictureboxhidden');
    choseAlbum.pageNumber=parseInt((allImageBox.length-1)/4+1);


    if(choseAlbum.pageNumber>currentPagesNumber){//add a page for each 4 pic
      const footerDom=choseAlbum.querySelector(".footer");//footer object
      const pageA=document.createElement("a");
      pageA.innerHTML=choseAlbum.pageNumber;
      pageA.href="#";
      pageA.index3=choseAlbum.pageNumber-1;
      console.log(pageA.index3);
      pageA.addEventListener("click",clickpage_handle)//listen
      footerDom.appendChild(pageA);
    }

    //when upload picture, the current page is the last page of the album,
    console.log(choseAlbum.pageNumber);
    renderpage(choseAlbum.pageNumber-1);

  }
  fr.readAsDataURL(file);//读取后 转换数据
    fileInput.value="";//实现一张图片连续多次上传

} )


function clickpage_handle(event){
  var index=event.target.index3;
  renderpage(index);
}
